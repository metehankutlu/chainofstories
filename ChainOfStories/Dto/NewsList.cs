﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Dto
{
    public class NewsList
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ShortDescription { get; set; }
        [Required(ErrorMessage = "Lütfen başlığı boş bırakmayın")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Lütfen açıklamayı boş bırakmayın")]
        public string Description { get; set; }
        public int LocaleId { get; set; }
        public int? Order { get; set; }
        public string Lang { get; set; }
    }
}
