﻿using System.ComponentModel.DataAnnotations;
namespace Dto
{
    public class SlidePhotoList
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsActive { get; set; }
        public int LocaleId { get; set; }
        public int? Order { get; set; }
        public string Lang { get; set; }
    }
}
