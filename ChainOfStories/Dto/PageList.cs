﻿namespace Dto
{
    public class PageList
    {
        public int Id { get; set; }
        public int LocaleId { get; set; }
        public string Lang { get; set; }
        public string Description { get; set; }
        public string PageName { get; set; }
    }
}
