﻿using System.Globalization;

namespace Facade
{
    public class FacadeBase
    {
        private readonly chainofstoriesEntities _entityModel;
        public chainofstoriesEntities EntityModel
        {
            get { return _entityModel ?? new chainofstoriesEntities(); }
        }

        public string Culture
        {
            get { return CultureInfo.CurrentUICulture.ToString(); }

        }

        public FacadeBase()
        {
            _entityModel = new chainofstoriesEntities();
        }
    }
}