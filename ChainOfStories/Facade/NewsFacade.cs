﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dto;

namespace Facade
{
    public class NewsFacade : FacadeBase
    {
        public IList<NewsList> GetAll(bool? isActive)
        {
            var data = (from p in EntityModel.News
                        join t in EntityModel.NewsLocale on p.Id equals t.NewsId
                        where t.Lang == Culture
                        orderby p.Order
                        select new NewsList()
                        {
                            Id = p.Id,
                            ImageUrl = t.ImageUrl,
                            IsActive = p.IsActive,
                            ShortDescription = t.ShortDescription,
                            Title = t.Title,
                            Description = t.Description,
                            CreateDate = p.CreateDate,
                            Order = p.Order,
                            LocaleId = t.Id
                        });

            if (isActive.HasValue)
                data = data.Where(x => x.IsActive == isActive.Value);

            return data.ToList();
        }


        public IList<NewsList> GetAllForUl()
        {
            var data = (from p in EntityModel.News
                        join t in EntityModel.NewsLocale on p.Id equals t.NewsId
                        where t.Lang == Culture && p.IsActive == true
                        orderby p.Order
                        select new NewsList()
                        {
                            Id = p.Id,
                            ImageUrl = t.ImageUrl,
                            IsActive = p.IsActive,
                            ShortDescription = t.ShortDescription,
                            Title = t.Title,
                            Description = t.Description,
                            CreateDate = p.CreateDate,
                            Order = p.Order,
                            LocaleId = t.Id
                        }).Take(4);


            return data.ToList();
        }

        public IList<NewsList> Search(string key)
        {
            var data = (from p in EntityModel.News
                        join t in EntityModel.NewsLocale on p.Id equals t.NewsId
                        where t.Lang == Culture && p.IsActive == true && (t.Title.Contains(key) || t.Description.Contains(key) || t.ShortDescription.Contains(key))
                        orderby p.Order
                        select new NewsList()
                        {
                            Id = p.Id,
                            ImageUrl = t.ImageUrl,
                            IsActive = p.IsActive,
                            ShortDescription = t.ShortDescription,
                            Title = t.Title,
                            Description = t.Description,
                            CreateDate = p.CreateDate,
                            Order = p.Order,
                            LocaleId = t.Id
                        });
            return data.ToList();
        }

        public void CreateNew(string photo, string title, string shortDescription, string description)
        {
            var news = new News()
            {
                CreateDate = DateTime.Now,
                Order = GetMaxOrder(),
                IsActive = false
            };

            EntityModel.News.Add(news);
            EntityModel.SaveChanges();


            foreach (var locale in EntityModel.Languages.ToList().Select(item => new NewsLocale()
            {
                NewsId = news.Id,
                Lang = item.LangCode,
                Description = description,
                ShortDescription = shortDescription,
                Title = title,
                ImageUrl = photo
            }))
            {
                EntityModel.NewsLocale.Add(locale);
            }
            EntityModel.SaveChanges();

        }

        private int GetMaxOrder()
        {
            if (EntityModel.News.Any())
                return EntityModel.News.Max(x => x.Order.Value) + 1;
            return 1;
        }

        public bool UpdateOrder(int newsId, bool isDown)
        {
            var newsItem = EntityModel.News.FirstOrDefault(x => x.Id == newsId);
            if (isDown)
            {
                var isThere = EntityModel.News.Any(x => x.Order > newsItem.Order);
                if (!isThere) return false;
                var items = EntityModel.News.Where(x => x.Order > newsItem.Order);
                var itemOrder = items.Min(x => x.Order);
                var item = items.FirstOrDefault(x => x.Order == itemOrder);
                if (newsItem != null)
                {
                    var order = newsItem.Order;
                    if (item != null)
                    {
                        newsItem.Order = item.Order;
                        item.Order = order;
                    }
                }
                EntityModel.SaveChanges();
                return true;
            }
            else
            {
                var isThere = EntityModel.News.Any(x => x.Order < newsItem.Order);
                if (!isThere) return false;
                var items = EntityModel.News.Where(x => x.Order < newsItem.Order);
                var itemOrder = items.Max(x => x.Order);
                var item = items.FirstOrDefault(x => x.Order == itemOrder);
                if (newsItem != null)
                {
                    var order = newsItem.Order;
                    if (item != null)
                    {
                        newsItem.Order = item.Order;
                        item.Order = order;
                    }
                }
                EntityModel.SaveChanges();
                return true;
            }

        }

        public IList<NewsList> GetNewsLocalesByNewsId(int id)
        {
            var data = (from p in EntityModel.News
                        join t in EntityModel.NewsLocale on p.Id equals t.NewsId
                        join q in EntityModel.Languages on t.Lang equals q.LangCode

                        where p.Id == id
                        select new NewsList()
                        {
                            LocaleId = t.Id,
                            ImageUrl = t.ImageUrl,
                            Order = p.Order,
                            Lang = t.Lang,
                            IsActive = p.IsActive,
                            Description = t.Description,
                            ShortDescription = t.ShortDescription,
                            Title = t.Title,
                            Id = p.Id
                        }).ToList();
            return data;
        }

        public bool DeleteItem(int newsId)
        {
            EntityModel.News.Remove(EntityModel.News.FirstOrDefault(x => x.Id == newsId));
            EntityModel.NewsLocale.Where(x => x.NewsId == newsId).ToList().ForEach(x => EntityModel.NewsLocale.Remove(x));
            EntityModel.SaveChanges();
            return true;
        }

        public NewsList GetNewsLocaleById(int newsLocaleId)
        {
            var data = (from p in EntityModel.News
                        join t in EntityModel.NewsLocale on p.Id equals t.NewsId
                        where t.Id == newsLocaleId
                        orderby p.Order
                        select new NewsList()
                        {
                            LocaleId = t.Id,
                            ImageUrl = t.ImageUrl,
                            Description = t.Description,
                            ShortDescription = t.ShortDescription,
                            Title = t.Title,
                            Id = p.Id,
                            IsActive = p.IsActive,
                            Lang = t.Lang,
                            Order = p.Order
                        }).FirstOrDefault();
            return data;
        }

        public void UpdateItem(NewsList item)
        {

            var updatedLocale = EntityModel.NewsLocale.FirstOrDefault(x => x.Id == item.LocaleId);
            var updateItem = EntityModel.News.FirstOrDefault(x => x.Id == updatedLocale.NewsId);

            if (updatedLocale != null)
            {
                updatedLocale.Description = item.Description;
                updatedLocale.ShortDescription = item.ShortDescription;
                updatedLocale.Title = item.Title;
                if (!string.IsNullOrWhiteSpace(item.ImageUrl))
                {
                    updatedLocale.ImageUrl = item.ImageUrl;
                }
            }

            EntityModel.SaveChanges();
        }

        public bool UpdateActive(int newsId)
        {
            var item = EntityModel.News.FirstOrDefault(x => x.Id == newsId);
            if (item != null) item.IsActive = !item.IsActive;
            EntityModel.SaveChanges();
            return true;
        }

        public News GetById(int newsId)
        {
            return EntityModel.News.FirstOrDefault(x => x.Id == newsId);
        }

    }
}
