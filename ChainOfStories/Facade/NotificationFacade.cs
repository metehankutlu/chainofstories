﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Facade
{
    public class NotificationFacade : FacadeBase
    {
        public IList<Notifications> GetAll(int writerId)
        {
            return EntityModel.Notifications.Where(x => x.WriterId == writerId).Take(6).ToList();
        }
        public void Create(int writerId, string title, string content, string url)
        {
            EntityModel.Notifications.Add(new Notifications()
            {
                WriterId = writerId,
                Title = title,
                Content = content,
                Url = url,
                CreateDate = DateTime.Now,
                Seen = false
            });
            EntityModel.SaveChanges();
        }
        public void See(int id)
        {
            EntityModel.Notifications.SingleOrDefault(x => x.Id == id).Seen = true;
            EntityModel.SaveChanges();
        }
    }
}