﻿using System.Collections.Generic;
using System.Linq;
using Dto;

namespace Facade
{
    public class PageFacade : FacadeBase
    {
        public IList<PageList> GetAll()
        {
            var data = (from p in EntityModel.Pages
                        join t in EntityModel.PagesLocale on p.Id equals t.PageId
                        where t.Lang == Culture
                        select new PageList()
                        {
                            Id = p.Id,
                            Description = t.Description,
                            PageName = t.PageName,
                            LocaleId = t.Id
                        });
            return data.ToList();
        }
        public PageList GetItem(int id)
        {
            var data = (from p in EntityModel.Pages
                        join t in EntityModel.PagesLocale on p.Id equals t.PageId
                        where t.Lang == Culture && p.Id == id
                        select new PageList()
                        {
                            Id = p.Id,
                            Description = t.Description,
                            PageName = t.PageName,
                            LocaleId = t.Id
                        }).FirstOrDefault();

            return data;
        }
        public PageList GetItemByPage(string page)
        {
            var data = (from p in EntityModel.Pages
                        join t in EntityModel.PagesLocale on p.Id equals t.PageId
                        where t.Lang == Culture && t.PageName == page
                        select new PageList()
                        {
                            Id = p.Id,
                            Description = t.Description,
                            PageName = t.PageName,
                            LocaleId = t.Id
                        }).FirstOrDefault();

            return data;
        }
        public void UpdateItem(PageList item)
        {

            var updatedLocale = EntityModel.PagesLocale.FirstOrDefault(x => x.Id == item.LocaleId);

            if (updatedLocale == null) return;
            updatedLocale.Description = item.Description;

            EntityModel.SaveChanges();
        }
        public IList<PageList> GetPageLocalesByPage(string page)
        {
            var data = (from p in EntityModel.Pages
                        join t in EntityModel.PagesLocale on p.Id equals t.PageId
                        join q in EntityModel.Languages on t.Lang equals q.LangCode
                        where t.PageName == page
                        select new PageList()
                        {
                            LocaleId = t.Id,
                            Lang = t.Lang,
                            PageName = t.PageName,
                            Description = t.Description,
                            Id = p.Id
                        }).ToList();
            return data;
        }
        public PageList GetPageLocaleById(int pageLocaleId)
        {
            var data = (from p in EntityModel.Pages
                        join t in EntityModel.PagesLocale on p.Id equals t.PageId
                        where t.Id == pageLocaleId
                        select new PageList()
                        {
                            LocaleId = t.Id,
                            Description = t.Description,
                            Id = p.Id,
                            PageName = t.PageName,
                            Lang = t.Lang,
                        }).FirstOrDefault();
            return data;
        }
    }
}
