﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Facade
{
    public class StoryFacade : FacadeBase
    {
        public IList<Story> GetAll(bool? isFull, bool? isConfirmed)
        {
            var list = EntityModel.Story.Where(x => x.Lang == Culture).OrderByDescending(x => x.CreateDate).ToList();
            if (isConfirmed.HasValue)
                list = list.Where(x => x.IsConfirmed == isConfirmed).ToList();
            if (isFull.HasValue)
                list = list.Where(x => x.IsFull == isFull).ToList();
            return list;
        }
        public Story GetStory(int id)
        {
            return EntityModel.Story.SingleOrDefault(x => x.Id == id);
        }
        public IList<Writer> GetWriters(int storyId)
        {
            var storyWriters = EntityModel.StoryWriter.Where(x => x.StoryId == storyId).ToList();
            List<Writer> writers = new List<Writer>();
            foreach (var item in storyWriters)
            {
                writers.Add(EntityModel.Writer.SingleOrDefault(x => x.Id == item.WriterId));
            }
            return writers;
        }
        public IList<StoryWriter> GetStoryWriters(int storyId)
        {
            return EntityModel.StoryWriter.Where(x => x.StoryId == storyId).OrderBy(x => x.Order).ToList();
        }
        public IList<Chain> GetChains(int storyWriterId)
        {
            return EntityModel.Chain.Where(x => x.StoryWriterId == storyWriterId).OrderBy(x => x.CreateDate).ToList();
        }
        public IList<Chain> GetAllChains(int storyId)
        {
            var story = GetStory(storyId);
            var storyWriters = GetStoryWriters(storyId);
            var chains = (from u in EntityModel.Chain
                          join v in EntityModel.StoryWriter on u.StoryWriterId equals v.Id
                          select u).OrderBy(x => x.CreateDate).ToList(); ;
            return chains;
        }
        public Chain GetChain(int id)
        {
            return EntityModel.Chain.SingleOrDefault(x => x.Id == id);
        }
        public int Create(string title, int turnCount, int capacity)
        {
            var story = new Story()
            {
                Title = title,
                TurnCount = turnCount,
                Turn = 1,
                Lang = Culture,
                Capacity = capacity,
                CreateDate = DateTime.Now,
                IsConfirmed = false,
                IsFull = false,
                WriterCount = 0,
                IsFinished = false
            };
            EntityModel.Story.Add(story);
            EntityModel.SaveChanges();
            return story.Id;
        }
        public void JoinWriter(int storyId, int writerId)
        {
            var story = GetStory(storyId);
            var storyWriter = new StoryWriter()
            {
                StoryId = storyId,
                Order = EntityModel.StoryWriter.Where(x => x.StoryId == storyId).Max(x => x.Order) + 1,
                WriterId = writerId
            };
            EntityModel.StoryWriter.Add(storyWriter);
            story.WriterCount++;
            if (story.Capacity == story.WriterCount)
            {
                story.IsFull = true;
            }
            EntityModel.SaveChanges();
        }
        public StoryWriter GetStoryWriter(int storyId, int writerId)
        {
            return EntityModel.StoryWriter.Single(x => x.StoryId == storyId && x.WriterId == writerId);
        }
        public void WriteChain(int storyId, int writerId, string text)
        {
            var story = GetStory(storyId);
            var storyWriterId = GetStoryWriter(story.Id, writerId).Id;
            var chain = new Chain()
            {
                StoryWriterId = storyWriterId,
                Text = text,
                CreateDate = DateTime.Now
            };
            EntityModel.Chain.Add(chain);
            story.Turn++;
            if (story.Turn > story.Capacity)
            {
                story.Turn = story.Turn.Value % story.Capacity.Value;
                story.TurnCount--;
            }
            if (story.TurnCount.Value == 0)
            {
                story.IsFinished = true;
            }
            EntityModel.SaveChanges();
        }
        public void LikeChain(int chainId, int writerId)
        {
            EntityModel.Likes.Add(new Likes()
            {
                ChainId = chainId,
                WriterId = writerId
            });
            EntityModel.SaveChanges();
        }
        public int GetLikeCount(int chainId)
        {
            return EntityModel.Likes.Where(x => x.ChainId == chainId).ToList().Count;
        }
        public IList<Writer> GetLikers(int chainId)
        {
            var likes = EntityModel.Likes.Where(x => x.ChainId == chainId).ToList();
            List<Writer> writers = new List<Writer>();
            foreach (var item in likes)
            {
                writers.Add(EntityModel.Writer.SingleOrDefault(x => x.Id == item.WriterId));
            }
            return writers;
        }
        public void Confirm(int storyId)
        {
            GetStory(storyId).IsConfirmed = true;
            EntityModel.SaveChanges();
        }
        public void Unconfirm(int storyId)
        {
            GetStory(storyId).IsConfirmed = false;
            EntityModel.SaveChanges();
        }
        public int GetTurn(int storyId)
        {
            var story = GetStory(storyId);
            var writers = GetWriters(storyId);
            return writers[story.Turn.Value - 1].Id;
        }
    }
}