﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Facade
{
    public class AdminFacade : FacadeBase
    {
        public bool CheckLogin(string userName, string passWord)
        {
            return EntityModel.Admin.Any(x => x.UserName == userName && x.Password == passWord);
        }
        public Admin GetLogin(string userName, string passWord)
        {
            return EntityModel.Admin.FirstOrDefault(x => x.UserName == userName && x.Password == passWord);
        }
    }
}