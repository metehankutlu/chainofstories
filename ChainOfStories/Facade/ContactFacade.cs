﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;

namespace Facade
{
    public class ContactFacade : FacadeBase
    {
        public IList<Contact> GetAll()
        {
            return EntityModel.Contact.ToList();
        }

        public void CreateContact(string name, string email, string subject, string message)
        {
            Contact contact = new Contact() { Name = name, Email = email, Subject = subject, Message = message, CreateDate = DateTime.Now };
            EntityModel.Contact.Add(contact);
            EntityModel.SaveChanges();
        }

        public void DeleteContact(int id)
        {
            EntityModel.Contact.Remove(EntityModel.Contact.Single(x => x.Id == id));
            EntityModel.SaveChanges();
        }

        public Contact GetById(int id)
        {
            return EntityModel.Contact.Single(x => x.Id == id);
        }
    }
}
