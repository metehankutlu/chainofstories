﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Facade
{
    public class WriterFacade : FacadeBase
    {
        public IList<Writer> GetAll()
        {
            return EntityModel.Writer.ToList();
        }
        public void Create(string username, string password, string email, string imageUrl, string lang)
        {
            EntityModel.Writer.Add(new Writer()
            {
                UserName = username,
                Password = password,
                Email = email,
                ImageUrl = imageUrl,
                Lang = lang,
                IsBanned = false,
                BanStart = null,
                BanEnd = null,
                BanCount = 0,
                SpamCount = 0,
                CreateDate = DateTime.Now,
                IsOnline = false
            });
            EntityModel.SaveChanges();
        }
        public Writer GetById(int id)
        {
            return EntityModel.Writer.SingleOrDefault(x => x.Id == id);
        }
        public void ChangeUserName(string username, int id)
        {
            GetById(id).UserName = username;
            EntityModel.SaveChanges();
        }
        public void ChangePassword(string password, int id)
        {
            GetById(id).Password = password;
            EntityModel.SaveChanges();
        }
        public void ChangeEmail(string email, int id)
        {
            GetById(id).Email = email;
            EntityModel.SaveChanges();
        }
        public void ChangeImageUrl(string url, int id)
        {
            GetById(id).ImageUrl = url;
            EntityModel.SaveChanges();
        }
        public bool IsOnline(int id)
        {
            return GetById(id).IsOnline.Value;
        }
        public void LoggedIn(int id)
        {
            GetById(id).IsOnline = true;
            EntityModel.SaveChanges();
        }
        public void LoggedOut(int id)
        {
            GetById(id).IsOnline = false;
            EntityModel.SaveChanges();
        }
        public void Ban(int id)
        {
            var writer = GetById(id);
            writer.BanCount++;
            writer.IsBanned = true;
            writer.BanStart = DateTime.Now;
            switch (writer.BanCount)
            {
                case 1: writer.BanEnd = DateTime.Now.AddDays(7); break;
                case 2: writer.BanEnd = DateTime.Now.AddMonths(1); break;
                case 3: writer.BanEnd = DateTime.MaxValue; break;
                default:
                    break;
            }
            EntityModel.SaveChanges();
        }
        public void RemoveBan(int id)
        {
            var writer = GetById(id);
            writer.IsBanned = false;
            writer.BanEnd= DateTime.Now;
            EntityModel.SaveChanges();
        }
        public void Spam(int id)
        {
            var writer = GetById(id);
            writer.SpamCount++;
            if (writer.SpamCount == 10)
            {
                Ban(id);
                writer.SpamCount = 0;
            }
            EntityModel.SaveChanges();
        }
        public bool CheckLogin(string userName, string passWord)
        {
            return EntityModel.Writer.Any(x => x.UserName == userName && x.Password == passWord);
        }
        public Writer GetLogin(string userName, string passWord)
        {
            return EntityModel.Writer.FirstOrDefault(x => x.UserName == userName && x.Password == passWord);
        }
    }
}