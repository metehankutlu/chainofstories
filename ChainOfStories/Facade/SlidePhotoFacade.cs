﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dto;

namespace Facade
{
    public class SlidePhotoFacade : FacadeBase
    {
        public IList<SlidePhotoList> GetAll(bool? isActive)
        {
            var data = (from p in EntityModel.SlidePhoto
                        join t in EntityModel.SlidePhotoLocale on p.Id equals t.SlidePhotoId
                        where t.Lang == Culture
                        orderby p.Order
                        select new SlidePhotoList()
                        {
                            Id = p.Id,
                            ImageUrl = t.ImageUrl,
                            IsActive = p.IsActive,
                            Order = p.Order,
                            LocaleId = t.Id
                        });

            if (isActive.HasValue)
                data = data.Where(x => x.IsActive == isActive.Value);

            return data.ToList();
        }
        public void CreateSlidePhoto(string photo)
        {
            var slidephoto = new SlidePhoto()
            {
                CreateDate = DateTime.Now,
                Order = GetMaxOrder(),
                IsActive = false
            };

            EntityModel.SlidePhoto.Add(slidephoto);
            EntityModel.SaveChanges();


            foreach (var locale in EntityModel.Languages.ToList().Select(item => new SlidePhotoLocale()
            {
                SlidePhotoId = slidephoto.Id,
                CreateDate = DateTime.Now,
                Lang = item.LangCode,
                ImageUrl = photo
            }))
            {
                EntityModel.SlidePhotoLocale.Add(locale);
            }
            EntityModel.SaveChanges();

        }

        private int GetMaxOrder()
        {
            if (EntityModel.SlidePhoto.Any())
                return EntityModel.SlidePhoto.Max(x => x.Order.Value) + 1;
            return 1;
        }

        public bool UpdateOrder(int slidePhotoId, bool isDown)
        {
            var slidePhotoItem = EntityModel.SlidePhoto.FirstOrDefault(x => x.Id == slidePhotoId);
            if (isDown)
            {
                var isThere = EntityModel.SlidePhoto.Any(x => x.Order > slidePhotoItem.Order);
                if (!isThere) return false;
                var items = EntityModel.SlidePhoto.Where(x => x.Order > slidePhotoItem.Order);
                var itemOrder = items.Min(x => x.Order);
                var item = items.FirstOrDefault(x => x.Order == itemOrder);
                if (slidePhotoItem != null)
                {
                    var order = slidePhotoItem.Order;
                    if (item != null)
                    {
                        slidePhotoItem.Order = item.Order;
                        item.Order = order;
                    }
                }
                EntityModel.SaveChanges();
                return true;
            }
            else
            {
                var isThere = EntityModel.SlidePhoto.Any(x => x.Order < slidePhotoItem.Order);
                if (!isThere) return false;
                var items = EntityModel.SlidePhoto.Where(x => x.Order < slidePhotoItem.Order);
                var itemOrder = items.Max(x => x.Order);
                var item = items.FirstOrDefault(x => x.Order == itemOrder);
                if (slidePhotoItem != null)
                {
                    var order = slidePhotoItem.Order;
                    if (item != null)
                    {
                        slidePhotoItem.Order = item.Order;
                        item.Order = order;
                    }
                }
                EntityModel.SaveChanges();
                return true;
            }

        }

        public IList<SlidePhotoList> GetSlidePhotoLocalesBySlidePhotoId(int id)
        {
            var data = (from p in EntityModel.SlidePhoto
                        join t in EntityModel.SlidePhotoLocale on p.Id equals t.SlidePhotoId
                        join q in EntityModel.Languages on t.Lang equals q.LangCode

                        where p.Id == id
                        select new SlidePhotoList()
                        {
                            LocaleId = t.Id,
                            ImageUrl = t.ImageUrl,
                            Order = p.Order,
                            Lang = t.Lang,
                            IsActive = p.IsActive,
                            Id = p.Id
                        }).ToList();
            return data;
        }

        public bool DeleteItem(int slidePhotoId)
        {
            EntityModel.SlidePhoto.Remove(EntityModel.SlidePhoto.FirstOrDefault(x => x.Id == slidePhotoId));
            EntityModel.SlidePhotoLocale.Where(x => x.SlidePhotoId == slidePhotoId).ToList().ForEach(x => EntityModel.SlidePhotoLocale.Remove(x));
            EntityModel.SaveChanges();
            return true;
        }

        public SlidePhotoList GetSlidePhotoLocaleById(int slidephotoLocaleId)
        {
            var data = (from p in EntityModel.SlidePhoto
                        join t in EntityModel.SlidePhotoLocale on p.Id equals t.SlidePhotoId
                        where t.Id == slidephotoLocaleId
                        orderby p.Order
                        select new SlidePhotoList()
                        {
                            LocaleId = t.Id,
                            ImageUrl = t.ImageUrl,
                            Id = p.Id,
                            IsActive = p.IsActive,
                            Lang = t.Lang,
                            Order = p.Order
                        }).FirstOrDefault();
            return data;
        }

        public void UpdateItem(SlidePhotoList item)
        {

            var updatedLocale = EntityModel.SlidePhotoLocale.FirstOrDefault(x => x.Id == item.LocaleId);
            var updateItem = EntityModel.SlidePhoto.FirstOrDefault(x => x.Id == updatedLocale.SlidePhotoId);

            if (updatedLocale != null)
            {
                if (!string.IsNullOrWhiteSpace(item.ImageUrl))
                    updatedLocale.ImageUrl = item.ImageUrl;
            }

            EntityModel.SaveChanges();
        }

        public bool UpdateActive(int slidephotoId)
        {
            var item = EntityModel.SlidePhoto.FirstOrDefault(x => x.Id == slidephotoId);
            if (item != null) item.IsActive = !item.IsActive;
            EntityModel.SaveChanges();
            return true;
        }

        public SlidePhoto GetById(int slidePhotoId)
        {
            return EntityModel.SlidePhoto.FirstOrDefault(x => x.Id == slidePhotoId);

        }

    }
}
