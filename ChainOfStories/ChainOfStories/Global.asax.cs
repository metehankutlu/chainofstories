﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Globalization;
using System.Threading;

namespace ChainOfStories
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        protected void Application_BeginRequest()
        {
            var httpCookie = Request.Cookies["CultureInfo"] ?? new HttpCookie("Culture", "tr-TR");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(httpCookie.Value);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(httpCookie.Value);
        }
    }
}
