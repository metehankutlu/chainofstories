﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChainOfStories.Controllers
{
    [AdminAuthorize]
    public class StoryController : BaseController
    {
        public ActionResult Index()
        {
            var stories = StoryFacade.GetAll(null, null);
            return View(stories);
        }
        public ActionResult Confirm(int id)
        {
            StoryFacade.Confirm(id);
            return RedirectToAction("/Index");
        }
        public ActionResult Unconfirm(int id)
        {
            StoryFacade.Unconfirm(id);
            return RedirectToAction("/Index");
        }
        public ActionResult Details(int id)
        {
            var story = StoryFacade.GetStory(id);
            ViewBag.Chains = StoryFacade.GetAllChains(id);
            ViewBag.Writers = StoryFacade.GetWriters(id);
            return View(story);
        }
    }
}