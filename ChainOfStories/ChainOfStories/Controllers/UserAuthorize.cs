﻿using System.Web;
using System.Web.Mvc;

namespace ChainOfStories.Controllers
{
    public class UserAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var httpCookie = httpContext.Request.Cookies["User"];
            if (httpCookie != null &&  httpCookie.Value != null)
            {
                return true;
            }
            httpContext.Response.Redirect("/Home/Login");
            return false;
        }
    }
}