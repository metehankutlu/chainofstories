﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChainOfStories.Controllers
{
    [AdminAuthorize]
    public class WriterController : BaseController
    {
        // GET: Writer
        public ActionResult Index()
        {
            return View(WriterFacade.GetAll());
        }

        public ActionResult Ban(int id)
        {
            WriterFacade.Ban(id);
            return RedirectToAction("/Index");
        }

        public ActionResult RemoveBan(int id)
        {
            WriterFacade.RemoveBan(id);
            return RedirectToAction("/Index");
        }
    }
}