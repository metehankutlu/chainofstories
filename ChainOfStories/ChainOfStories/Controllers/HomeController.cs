﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Dto;
using Facade;
using ChainOfStories.Models;
using System.Web.Security;

namespace ChainOfStories.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Slide = SlidePhotoFacade.GetAll(true);
            ViewBag.News = NewsFacade.GetAllForUl();
            return View();
        }
        public ActionResult AboutUs()
        {
            var contactPage = PageFacade.GetItemByPage("AboutUs");
            return View(contactPage);
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult CreateContact(string name, string email, string subject, string message)
        {
            ContactFacade.CreateContact(name, email, subject, message);
            return RedirectToAction("Contact", "Home");
        }
        public ActionResult News(int page = 1, int pageSize = 3)
        {
            var news = NewsFacade.GetAll(true);
            var model = new PagedList<NewsList>(news, page, pageSize);
            ViewBag.Stories = StoryFacade.GetAll(false, null).Take(6);
            ViewBag.AllNews = news;
            return View(model);
        }
        public ActionResult NewsDetails(int id)
        {
            var news = NewsFacade.GetNewsLocaleById(id);
            return View(news);
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string userName, string passWord)
        {
            if (!WriterFacade.CheckLogin(userName, passWord) || (userName == "" || passWord == ""))
            {
                ModelState.AddModelError("UserWrong", "Yanlış kullanıcı/şifre");
                return View();
            }
            FormsAuthentication.SetAuthCookie(userName, true);
            var userCookie = new HttpCookie("User") { Value = WriterFacade.GetLogin(userName, passWord).Id.ToString(), Expires = DateTime.Now.AddYears(1) };
            Response.Cookies.Add(userCookie);
            return RedirectToAction("/Index");
        }
        public Writer GetCurrentWriter()
        {
            return WriterFacade.GetById(Convert.ToInt32(Request.Cookies["User"].Value));
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            var httpCookie = Response.Cookies["User"];
            httpCookie.Expires = DateTime.Now;
            return RedirectToAction("Login");
        }
        public ActionResult Register()
        {
            chainofstoriesEntities db = new chainofstoriesEntities();
            ViewBag.Lang = new SelectList(db.Languages, "LangCode", "Lang");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Register([Bind(Include = "UserName, Password, Email, Lang")]Writer writer, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                var imageUrl = UploadImage(image);
                WriterFacade.Create(writer.UserName, writer.Password, writer.Email, imageUrl, writer.Lang);
                return RedirectToAction("/Index");
            }
            if (image == null)
            {
                ModelState.AddModelError("ImageValidationError", "Lütfen resim ekleyiniz");
            }
            return View(writer);
        }

        public ActionResult AdminLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AdminLogin(string UserName, string Password)
        {
            if (!AdminFacade.CheckLogin(UserName, Password) || (UserName == "" || Password == ""))
            {
                ModelState.AddModelError("UserWrong", "Yanlış kullanıcı/şifre");
                return View();
            }
            FormsAuthentication.SetAuthCookie(UserName, true);
            var userCookie = new HttpCookie("Admin") { Value = AdminFacade.GetLogin(UserName, Password).Id.ToString(), Expires = DateTime.Now.AddYears(1) };
            Response.Cookies.Add(userCookie);
            return RedirectToAction("/AdminIndex");
        }
        public ActionResult AdminLogOut()
        {
            FormsAuthentication.SignOut();
            var httpCookie = Response.Cookies["Admin"];
            if (httpCookie != null)
                httpCookie.Value = null;
            httpCookie.Expires = DateTime.Now;
            return RedirectToAction("AdminLogin");
        }
        [AdminAuthorize]
        public ActionResult AdminIndex()
        {
            return View();
        }
        public ActionResult Stories()
        {
            var stories = StoryFacade.GetAll(null, null);
            List<StoryViewModel> list = new List<StoryViewModel>();
            for (int i = 0; i < stories.Count; i++)
            {
                IList<Writer> writers;
                Writer writer;
                Writer isJoined = new Writer();
                if (Request.Cookies["User"] != null)
                {
                    writers = StoryFacade.GetWriters(stories[i].Id);
                    writer = WriterFacade.GetById(Convert.ToInt32(Request.Cookies["User"].Value));
                    isJoined = writers.Where(x => x.Id == writer.Id).FirstOrDefault();
                }
                list.Add(new StoryViewModel()
                {
                    story = stories[i],
                    writers = StoryFacade.GetWriters(stories[i].Id),
                    IsJoined = isJoined == null ? false : true
                });
            }
            return View(list);
        }
        [UserAuthorize]
        public ActionResult JoinStory(int id)
        {
            Writer user = WriterFacade.GetById(Convert.ToInt32(Request.Cookies["User"].Value));
            StoryFacade.JoinWriter(id, user.Id);
            return RedirectToAction("Story", new { id = id });
        }
        [UserAuthorize]
        public ActionResult Write(int storyId,string text)
        {
            Writer user = WriterFacade.GetById(Convert.ToInt32(Request.Cookies["User"].Value));
            StoryFacade.WriteChain(storyId, user.Id, text);
            return RedirectToAction("Story", new { id = storyId });
        }
        [UserAuthorize]
        public ActionResult Story(int id)
        {
            var story = StoryFacade.GetStory(id);
            ViewBag.Writers = StoryFacade.GetWriters(id);
            ViewBag.Chains = StoryFacade.GetAllChains(id);
            ViewBag.CurrentWriter = StoryFacade.GetTurn(id);
            var writer = WriterFacade.GetById(StoryFacade.GetTurn(id)).Id.ToString();
            if (writer == Request.Cookies["User"].Value && story.IsFull.Value && !story.IsFinished.Value)
                ViewBag.Write = true;
            else
                ViewBag.Write = false;
            return View(story);
        }
        [UserAuthorize]
        public ActionResult CreateStory()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorize]
        public ActionResult CreateStory(string Title, int TurnCount, int Capacity)
        {
            if (ModelState.IsValid)
            {
                int id = StoryFacade.Create(Title, TurnCount, Capacity);
                StoryFacade.JoinWriter(id, GetCurrentWriter().Id);
                return RedirectToAction("Story", new { id = id});
            }
            return View();
        }
    }
}