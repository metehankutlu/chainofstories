﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChainOfStories.Controllers
{
    public class CultureController : Controller
    {
        public ActionResult SetCulture(string actName, string lang, int? id)
        {
            var cookie = new HttpCookie("CultureInfo")
            {
                Value = lang
            };
            Response.Cookies.Add(cookie);
            return RedirectToAction(actName, "Home", new { id = id });
        }
    }
}