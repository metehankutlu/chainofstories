﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dto;

namespace ChainOfStories.Controllers
{
    [AdminAuthorize]
    public class PageController : BaseController
    {
        public ActionResult About()
        {
            var aboutPage = PageFacade.GetPageLocalesByPage("AboutUs");
            return View(aboutPage);
        }
        public ActionResult Edit(int id)
        {
            var page = PageFacade.GetPageLocaleById(id);
            return View(page);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, [Bind(Include = "Description")]PageList page)
        {
            if (!ModelState.IsValid)
            {
                var currentPage = PageFacade.GetItem(id);
                return View(currentPage);
            }
            page.LocaleId = id;
            PageFacade.UpdateItem(page);
            return RedirectToAction("/About");
        }
    }
}