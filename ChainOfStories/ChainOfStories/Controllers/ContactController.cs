﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChainOfStories.Controllers
{
    [AdminAuthorize]
    [ValidateInput(false)]
    public class ContactController : BaseController
    {
        // GET: Contact
        public ActionResult Index()
        {
            var contacts = ContactFacade.GetAll();
            return View(contacts);
        }

        public ActionResult Delete(int id)
        {
            ContactFacade.DeleteContact(id);
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            var contact = ContactFacade.GetById(id);
            return View(contact);
        }
    }
}