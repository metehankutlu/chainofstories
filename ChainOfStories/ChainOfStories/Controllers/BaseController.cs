﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dto;
using Facade;
using System.IO;

namespace ChainOfStories.Controllers
{
    public class BaseController : Controller
    {
        ContactFacade _contactFacade;
        NewsFacade _newsFacade;
        PageFacade _pageFacade;
        SlidePhotoFacade _slidePhotoFacade;
        StoryFacade _storyFacade;
        WriterFacade _writerFacade;
        AdminFacade _adminFacade;
        public ContactFacade ContactFacade
        {
            get { return _contactFacade ?? (_contactFacade = new ContactFacade()); }
        }
        public SlidePhotoFacade SlidePhotoFacade
        {
            get { return _slidePhotoFacade ?? (_slidePhotoFacade = new SlidePhotoFacade()); }
        }
        public StoryFacade StoryFacade
        {
            get { return _storyFacade ?? (_storyFacade = new StoryFacade()); }
        }
        public WriterFacade WriterFacade
        {
            get { return _writerFacade ?? (_writerFacade = new WriterFacade()); }
        }
        public NewsFacade NewsFacade
        {
            get { return _newsFacade ?? (_newsFacade = new NewsFacade()); }
        }
        public PageFacade PageFacade
        {
            get { return _pageFacade ?? (_pageFacade = new PageFacade()); }
        }
        public AdminFacade AdminFacade
        {
            get { return _adminFacade ?? (_adminFacade = new AdminFacade()); }
        }
        public string UploadImage(HttpPostedFileBase image)
        {
            var imagePath = Path.Combine(Server.MapPath("~/Content/"), image.FileName);
            var imageUrl = Path.Combine("~/Content/", image.FileName);
            while (System.IO.File.Exists(imagePath))
            {
                Random rnd = new Random();
                string fileName = Path.GetFileNameWithoutExtension(image.FileName) + "-" + rnd.Next(0, 999) + Path.GetExtension(image.FileName);
                imagePath = Path.Combine(Server.MapPath("~/Content/"), fileName);
                imageUrl = Path.Combine("~/Content/", fileName);
            }
            image.SaveAs(imagePath);
            return imageUrl;
        }
    }
}