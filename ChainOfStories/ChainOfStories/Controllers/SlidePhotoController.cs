﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dto;

namespace ChainOfStories.Controllers
{
    [AdminAuthorize]
    public class SlidePhotoController : BaseController
    {
        public ActionResult Index()
        {
            var photos = SlidePhotoFacade.GetAll(null);
            return View(photos);
        }

        public ActionResult Details(int id)
        {
            var photos = SlidePhotoFacade.GetSlidePhotoLocalesBySlidePhotoId(id);
            return View(photos);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(SlidePhotoList slidephoto, HttpPostedFileBase image)
        {
            if (!ModelState.IsValid)
            {
                if (image == null)
                {
                    ModelState.AddModelError("ImageValidationError", "Lütfen resim ekleyiniz");
                }
                return View(slidephoto);
            }
            var imageUrl = UploadImage(image);
            SlidePhotoFacade.CreateSlidePhoto(imageUrl);
            return RedirectToAction("/Index");
        }

        public ActionResult Edit(int id)
        {
            var photo = SlidePhotoFacade.GetSlidePhotoLocaleById(id);
            return View(photo);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, SlidePhotoList slidephoto, HttpPostedFileBase image)
        {
            if (!ModelState.IsValid)
            {
                var currentPhoto = SlidePhotoFacade.GetSlidePhotoLocaleById(id);
                return View(currentPhoto);
            }
            slidephoto.LocaleId = id;
            if (image != null)
            {
                var imageUrl = UploadImage(image);
                slidephoto.ImageUrl = imageUrl;
            }
            SlidePhotoFacade.UpdateItem(slidephoto);
            return RedirectToAction("/Index");
        }

        public ActionResult Delete(int id)
        {
            SlidePhotoFacade.DeleteItem(id);
            return RedirectToAction("/Index", "SlidePhoto");
        }
        public ActionResult Active(int id)
        {
            SlidePhotoFacade.UpdateActive(id);
            return RedirectToAction("/Index");
        }
        public ActionResult Order(int id, bool isDown)
        {
            SlidePhotoFacade.UpdateOrder(id, isDown);
            return RedirectToAction("/Index");
        }
    }
}