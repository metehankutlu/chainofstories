﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChainOfStories.Controllers
{
    public class AdminAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var httpCookie = httpContext.Request.Cookies["Admin"];
            if (httpCookie != null && httpCookie.Value != null)
            {
                return true;
            }
            httpContext.Response.Redirect("/Home/AdminLogin");
            return false;
        }
    }
}