﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dto;

namespace ChainOfStories.Controllers
{
    [AdminAuthorize]
    public class NewsController : BaseController
    {
        public ActionResult Index()
        {
            var news = NewsFacade.GetAll(null);
            return View(news);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Title, Description, ShortDescription")]NewsList New, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                var imageUrl = UploadImage(image);
                NewsFacade.CreateNew(imageUrl, New.Title, New.ShortDescription, New.Description);
                return RedirectToAction("/Index");
            }
            if (image == null)
            {
                ModelState.AddModelError("ImageValidationError", "Lütfen resim ekleyiniz");
            }
            return View(New);
        }
        public ActionResult Edit(int id)
        {
            var New = NewsFacade.GetNewsLocaleById(id);
            return View(New);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, [Bind(Include = "Description, ShortDescription, Title")]NewsList New, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                New.LocaleId = id;
                if (image != null)
                {
                    var imageUrl = UploadImage(image);
                    New.ImageUrl = imageUrl;
                }
                NewsFacade.UpdateItem(New);
                return RedirectToAction("/Index");
            }
            var currentNew = NewsFacade.GetNewsLocaleById(id);
            return View(currentNew);
        }
        public ActionResult Details(int id)
        {
            var New = NewsFacade.GetNewsLocalesByNewsId(id);
            return View(New);
        }
        public ActionResult Delete(int id)
        {
            NewsFacade.DeleteItem(id);
            return RedirectToAction("/Index", "News");
        }

        public ActionResult Order(int id, bool isDown)
        {
            NewsFacade.UpdateOrder(id, isDown);
            return RedirectToAction("/Index");
        }

        public ActionResult Active(int id)
        {
            NewsFacade.UpdateActive(id);
            return RedirectToAction("/Index");
        }
    }
}