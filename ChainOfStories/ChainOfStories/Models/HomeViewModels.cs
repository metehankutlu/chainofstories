﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Facade;

namespace ChainOfStories.Models
{
    public class StoryViewModel
    {
        public Story story { get; set; }
        public IList<Writer> writers { get; set; }
        public bool IsJoined { get; set; }
    }
}